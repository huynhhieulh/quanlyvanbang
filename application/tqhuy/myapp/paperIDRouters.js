var express = require('express');
var router = express.Router();

router.get('/', function(req,res){
    res.send('Ban da truy cap dia chi /paperID bang phuong thuc GET');
});
router.post('/',function(req,res){
    res.send('Ban da truy cap dia chi /paperID bang phuong thuc POST');
});

//Xuat bo dinh tuyen (router) nay de co the su dung o file khac
module.exports = router;