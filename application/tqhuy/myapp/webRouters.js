var express = require('express');
var webR = express();

webR.set('views','./views');
webR.set('view engine', 'pug');
webR.use(express.static('css'));
webR.get('/', function(req,res){
    res.render('users/index');
});
module.exports = webR;
//webR.listen(3000);