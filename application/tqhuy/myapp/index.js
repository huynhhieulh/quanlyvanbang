var express = require('express');
var app = express();

app.get('/',(req,res) => res.send('Hello World!'));
app.post('/', function(req, res){
    res.send("Ban vua gui yeu cua bang phuong thuc POST toi dia chi /hello");
 });

 var paperIDRouters = require('./paperIDRouters');
 app.use('/paperID', paperIDRouters);

// app.set('views','./views');
 //app.set('views engine','pug');
 var dashboardRouters = require('./webRouters');
 app.use('/dashboard', dashboardRouters);

app.listen(3000);