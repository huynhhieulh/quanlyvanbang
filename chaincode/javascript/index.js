/*
 * SPDX-License-Identifier: Apache-2.0
 */

'use strict';

const FabCar = require('./qlvb');

module.exports.qlvb = qlvb;
module.exports.contracts = [ qlvb ];
