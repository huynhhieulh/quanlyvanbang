/*
 * SPDX-License-Identifier: Apache-2.0
 */

'use strict';

const { Contract } = require('fabric-contract-api');

class qlvb extends Contract {

    async addDip(ctx, id, nameIn, ownerIn, dateIn, issuedByIn){
        let Dip={
            name: nameIn,
            owner: ownerIn,
            date: dateIn,
            issuedBy: issuedByIn
        };
        await ctx.stub.putState(id,Buffer.from(JSON.stringify(Dip)));
        console.info("Diploma added to the Ledger Succesfully..");

    }
    async queryDip(ctx, id){
        let dipsAsBytes = await ctx.stub.getState(id);
        if(!dipsAsBytes || dipsAsBytes.toString().length <= 0 ){
            throw new Error('Diploma with this ID does not exist..');
        }
        let dip = JSON.parse(dipsAsBytes.toString());
        return JSON.stringify(dip);
    }
    async deleteDip(ctx,id){
        let dipsAsBytes = await ctx.stub.getState(id);
        if(!dipsAsBytes || dipsAsBytes.toString().length <= 0 ){
            throw new Error('This ID does not exist..');
        }
        await ctx.stub.deleteState(id);
        console.info("Diploma deleted to the Ledger Succesfully..");
    }
    async changeOwner(ctx,id,newOwner){
        let dipsAsBytes = await ctx.stub.getState(id);
        if(!dipsAsBytes || dipsAsBytes.toString().length <= 0 ){
            throw new Error('This ID does not exist..');
        }
        let dip = JSON.parse(dipsAsBytes.toString());
        dip.owner = newOwner;
        await ctx.stub.putState(id,Buffer.from(JSON.stringify(dip)));
        console.info("Owner was changed to the Ledger Succesfully..");       
    }

}

module.exports = qlvb;
